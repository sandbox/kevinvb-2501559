<?php

class Teamleader_company {
  protected $name;
  protected $email;
  protected $vat_code;
  protected $telephone;
  protected $country;
  protected $zipcode;
  protected $city;
  protected $street;
  protected $number;
  protected $account_manager_id;
  protected $business_type;
  protected $language;
  protected $add_tag_by_string;
  protected $automerge_by_name;
  protected $automerge_by_mail;
  protected $automerge_by_vat_code;
  protected $custom_fields;

  public function __construct(string $name, array $values) {

  }
}
