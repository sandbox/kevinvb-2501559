<?php


class Teamleader_Contact {
  protected $forename;
  protected $surname;
  protected $email;

  protected $telephone;
  protected $gsm;
  protected $country;
  protected $zipcode;
  protected $city;
  protected $street;
  protected $number;
  protected $language;
  protected $gender;
  protected $dob;
  protected $newsletter;
  protected $add_tag_by_string;
  protected $automerge_by_name;
  protected $automerge_by_mail;
  protected $custom_fields;

  public function __construct(string $forename, string $surname, string $email, array $values = array()) {

  }
}
