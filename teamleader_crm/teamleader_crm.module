<?php
/**
 * Adds a contact to Teamleader
 * @param data is an array with:
 * key = parameter name
 * value = parameter value
 */
function teamleader_crm_add_contact($data) {
  module_load_include('inc', 'teamleader_connection', 'teamleader_connection.api');

  $url = "https://www.teamleader.be/api/addContact.php";

  $fields = teamleader_connection_get_postfields($data);
  $fields['track_changes'] = variable_get('teamleader_connection_track');

  $ch = teamleader_connection_prepare_curl($url, $fields);

  $api_output = curl_exec($ch);
  $json_output = json_decode($api_output);

  curl_close($ch);

  if($json_output) {
    return $json_output;
  }
  else{
    return FALSE;
  }
}

/**
 * Update an existing Teamleader customer
 */
function teamleader_crm_update_contact($data) {
  module_load_include('inc', 'teamleader_connection', 'teamleader_connection.api');

  $url = "https://www.teamleader.be/api/updateContact.php";

  $fields = teamleader_connection_get_postfields($data);
  $fields['track_changes'] = variable_get('teamleader_connection_track');

  $ch = teamleader_connection_prepare_curl($url, $fields);

  $api_output = curl_exec($ch);
  $json_output = json_decode($api_output);

  curl_close($ch);

  if($json_output) {
    return $json_output;
  }
  else{
    return FALSE;
  }
}

/**
 * Delete a contact from teamleader
 * @param  int $contact_id The teamleader contact id.
 * @return boolean         Returns if the deletion has been succeeded.
 */
function teamleader_crm_delete_contact($contact_id) {
  module_load_include('inc', 'teamleader_connection', 'teamleader_connection.api');

  $url = "https://www.teamleader.be/api/deleteContact.php";

  $data = array();
  $data['contact_id'] = $contact_id;

  $fields = teamleader_connection_get_postfields($data);

  $ch = teamleader_connection_prepare_curl($url, $fields);

  $api_output = curl_exec($ch);
  $json_output = json_decode($api_output);

  curl_close($ch);

  if($json_output) {
    return TRUE;
  }
  else{
    return FALSE;
  }
}

/**
 * Link or unlink a contact to a company
 * @param  int $contact_id     The Teamleader contact id.
 * @param  int $company_id     The Teamleader company id.
 * @param  string $mode        The mode, 'link' or 'unlink'.
 * @param  string $function    Optional job title the contact holds at the company. (eg. HR manager)
 * @return array               Json array, OK or error message
 */
function teamleader_crm_link_contact($contact_id, $company_id, $mode = 'link', $function = NULL) {
  module_load_include('inc', 'teamleader_connection', 'teamleader_connection.api');

  $url = "https://www.teamleader.be/api/linkContactToCompany.php";

  $data = array();
  $data['contact_id'] = $contact_id;
  $data['company_id'] = $company_id;
  $data['mode'] = $mode;
  if($function !== NULL) {
    $data['function'] = $function;
  }

  $fields = teamleader_connection_get_postfields($data);

  $ch = teamleader_connection_prepare_curl($url, $fields);

  $api_output = curl_exec($ch);
  $json_output = json_decode($api_output);

  curl_close($ch);

  if($json_output) {
    return $json_output;
  }
  else{
    return FALSE;
  }
}
/**
 * Search a contact in teamleader
 * @param  string  $term           The search term
 * @param  integer $amount         The amount of result to be returned, max 100.
 * @param  integer $pageno         The page number to start searching.
 * @param  date  $modified_since   The modification date.
 * @param  string  $filter_by_tag  Extra filtering e.g. 'lead'.
 * @return array                   Json array of contacts.
 */
function teamleader_crm_search_contact($term, $amount = 1, $pageno = 0, $modified_since = NULL, $filter_by_tag = NULL) {
  module_load_include('inc', 'teamleader_connection', 'teamleader_connection.api');

  $url = "https://www.teamleader.be/api/getContacts.php";

  $data = teamleader_connection_get_postfields();
  $data['amount'] = $amount;
  $data['pageno'] = $pageno;
  if($modified_since !== NULL)
    $data['modifiedsince'] = $modified_since;
  if($filter_by_tag !== NULL)
    $data['filter_by_tag'] = $filter_by_tag;

  $data['searchby'] = $term;

  $fields = teamleader_connection_get_postfields($data);

  $ch = teamleader_connection_prepare_curl($url, $fields);

  $api_output = curl_exec($ch);
  $json_output = json_decode($api_output);

  curl_close($ch);

  if($json_output) {
    return $json_output;
  }
  else{
    return FALSE;
  }
}

/**
 * Fetch a the contact information.
 * @param  int $contact_id   The Teamleader contact id to be fetched.
 * @return array             Json array containing the info about the contact
 */
function teamleader_crm_fetch_contact($contact_id) {
  module_load_include('inc', 'teamleader_connection', 'teamleader_connection.api');

  $url = "https://www.teamleader.be/api/getContact.php";

  $data = array();
  $data['contact_id'] = $contact_id;

  $fields = teamleader_connection_get_postfields($data);

  $ch = teamleader_connection_prepare_curl($url, $fields);

  $api_output = curl_exec($ch);
  $json_output = json_decode($api_output);

  curl_close($ch);

  if($json_output) {
    return $json_output;
  }
  else{
    return FALSE;
  }
}

function teamleader_crm_get_contacts($company_id) {
  module_load_include('inc', 'teamleader_connection', 'teamleader_connection.api');

  $url = "https://www.teamleader.be/api/getContactsByCompany.php";

  $data = array();
  $data['company_id'] = $company_id;

  $fields = teamleader_connection_get_postfields($data);

  $ch = teamleader_connection_prepare_curl($url, $fields);

  $api_output = curl_exec($ch);
  $json_output = json_decode($api_output);

  curl_close($ch);

  if($json_output) {
    return $json_output;
  }
  else{
    return FALSE;
  }
}

function teamleader_crm_add_company($data) {

}

function teamleader_crm_updata_company($data) {

}

function teamleader_crm_delete_company($data) {

}

function teamleader_crm_search_company($term, $amount = 10, $pageno = 0, $modified_since = NULL, $filter_by_tag = NULL) {

}

function teamleader_crm_fetch_company($company_id) {

}

function teamleader_crm_get_business_types($country) {

}
