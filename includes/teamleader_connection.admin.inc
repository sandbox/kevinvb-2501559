<?php

/**
 * Implements hook_form
 * Creates the teamleader settings page
 */
function teamleader_connection_settings_form($form, $form_state) {
  $form['api_group'] = array(
    '#type' => 'textfield',
    '#title' => t('Api group'),
    '#default_value' => variable_get('teamleader_connection_group', ''),
    '#required' => TRUE,
  );
  $form['api_key'] = array(
    '#type' => 'textarea',
    '#title' => t('Api key'),
    '#default_value' => variable_get('teamleader_connection_api', ''),
    '#required' => TRUE,
  );
  $form['track_changes'] = array(
    '#type' => 'checkbox',
    '#title' => t('Track update changes'),
    '#default_value' => variable_get('teamleader_connection_track', 1),
    '#description' => t('Teamleader will track any update changes you make in the forms of Drupal'),
  );
  $attributes = array(
    'attributes' => array(
      'target' => '_blank',
    ),
  );
  $form['api_markup'] = array(
    '#markup' => '<p>' . t('You can find your teamleader API key at: ') . l('teamleader.be', 'https://www.teamleader.be/apiwebhooks.php?show_key', $attributes) . '</p>',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

/**
 * Implements hook_form_submit
 */
function teamleader_connection_settings_form_submit($form, $form_state) {
  module_load_include('inc', 'teamleader_connection', 'teamleader_connection.api');
  $group = $form_state['values']['api_group'];
  $api_key = $form_state['values']['api_key'];
  $track = $form_state['values']['track_changes'];

  variable_set('teamleader_connection_track', $track);
  variable_set('teamleader_connection_group', $group);
  variable_set('teamleader_connection_api', $api_key);

  if(teamleader_connection_simple_connect()) {
    drupal_set_message(t('We could succesfully connect with Teamleader!'));
  }
  else {
    drupal_set_message(t('Sorry we could not connect with Teamleader'), 'warning');
  }
}
