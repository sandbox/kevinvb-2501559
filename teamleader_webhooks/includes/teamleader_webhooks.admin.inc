<?php

function teamleader_webhooks_admin($form, $form_state) {

  $query = db_select('teamleader_webhook_events', 'twe')
    ->fields('twe', array('eid', 'machine_name', 'name', 'bundle'))
    ->orderBy('bundle', 'ASC');
  $events = $query->execute()->fetchAll();

  $options = array();
  foreach($events as $event) {
    $options[$event->machine_name] = t($event->name);
  }

  $query = db_select('teamleader_webhook_events', 'twe');
  $query->join('teamleader_webhook_config', 'twc', 'twc.eid = twe.eid');
  $query->join('teamleader_webhook_actions', 'twa', 'twa.aid = twc.aid');
  $query->fields('twe', array('eid', 'machine_name', 'name', 'bundle'));
  $query->fields('twa', array('aid', 'machine_name', 'name'));
  $query->fields('twc', array('cid', 'label', 'machine_name', 'status', 'weight'));
  $query->orderBy('weight', 'ASC');

  $configurations = $query->execute()->fetchAll();
  $rows = array();

  foreach($configurations as $config) {
    if(!isset($form['configuration'][$config->bundle])) {
      $form['configuration'][$config->bundle] = array(
        '#type' => 'fieldset',
        '#title' => t($config->bundle),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      );
      $form['configuration'][$config->bundle]['config']['#tree'] = TRUE;
    }
    $form['configuration'][$config->bundle]['config'][$config->cid] = array(
      'select' => array(
        '#type' => 'checkbox',
      ),
      'label' => array(
        '#markup' => check_plain($config->label),
      ),
      'event' => array(
        '#markup' => check_plain($config->name),
      ),
      'weight' => array(
        '#type' => 'weight',
        '#title' => t('Weight'),
        '#default_value' => $config->weight,
        '#delta' => 10,
        '#title_display' => 'invisible',
      ),
      'edit' => array(
        '#markup' => l(t('Edit'), 'admin/config/teamleader/webhooks/config/'.$config->cid),
      ),
    );
  }
  $form['add_config'] = array(
    '#markup' => l('Add new action', '/admin/config/teamleader/webhooks/add'),
  );


   $form['actions'] = array('#type' => 'actions');

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

function theme_teamleader_webhooks_admin($variables) {
  $form = $variables['form'];
  $rows = array();
  $output = '';
  foreach(element_children($form['configuration']) as $id) {
    foreach(element_children($form['configuration'][$id]['config']) as $cid) {
      $form['configuration'][$id]['config'][$cid]['weight']['#attributes']['class'] = array('table-item-weight');
      $rows[] = array(
        'data' => array(
          drupal_render($form['configuration'][$id]['config'][$cid]['select']),
          drupal_render($form['configuration'][$id]['config'][$cid]['label']),
          drupal_render($form['configuration'][$id]['config'][$cid]['event']),
          drupal_render($form['configuration'][$id]['config'][$cid]['weight']),
          drupal_render($form['configuration'][$id]['config'][$cid]['edit']),
        ),
        'class' => array('draggable'),
      );
    }
    $header = array(t('Select'), t('Label'), t('Event'), t('Weight'), t('Action'));
    $table_id = 'table_items_'.$id;
    $markup = theme('table', array(
      'header' => $header,
      'rows' => $rows,
      'attributes' => array('id' => $table_id),
    ));
    $form['configuration'][$id]['config'] = array(
      '#markup' => $markup,
    );
    $output .= drupal_render($form['configuration'][$id]);

    $rows = array();
    drupal_add_tabledrag($table_id, 'order', 'sibling', 'table-item-weight');
  }
  $output .= drupal_render_children($form);

  return $output;
}

function teamleader_webhooks_admin_submit($form, $form_state) {

}

function teamleader_webhooks_add($form, &$form_state) {
  $query = db_select('teamleader_webhook_events', 'twe')
    ->fields('twe', array('eid', 'machine_name', 'name', 'bundle'))
    ->orderBy('bundle', 'ASC');
  $events = $query->execute()->fetchAll();

  $query = db_select('teamleader_webhook_actions', 'twa')
    ->fields('twa', array('aid', 'machine_name', 'name'))
    ->orderBy('Name', 'ASC');

  $actions = $query->execute()->fetchAll();

  $bundles = array();
  foreach($events as $event) {
    $bundles[$event->bundle] = $event->bundle;
  }

  $actions_options = array();
  foreach($actions as $action) {
    $actions_options[$action->aid] = t($action->name);
  }

  $selected = isset($form_state['values']['bundle_filter']) ? $form_state['values']['bundle_filter'] : key($bundles);

  $label = isset($form_state['values']['label']) ? $form_state['values']['label'] : '';

  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#default_value' => $label,
    '#required' => TRUE,
  );
  $form['machine_name'] = array(
    '#type' => 'machine_name',
    '#default_value' => 'webhook-' . $label,
    '#maxlength' => 21,
    '#description' => t('A unique machine-readable name containing letters, numbers, and underscores.'),
    '#machine_name' => array(
      'exists' => 'teamleader_webhooks_machine_name_exists',
      'replace_pattern' => '[^a-z0-9-]+',
      'replace' => '-',
    ),
  );
  $form['bundle_filter'] = array(
    '#type' => 'select',
    '#title' => t('Filter based on group'),
    '#options' => $bundles,
    '#default_value' => $selected,
    '#ajax' => array(
      'callback' => 'ajax_bundle_dropdown_callback',
      'wrapper' => 'bundle-filter',
    ),
  );
  $form['selected_event'] = array(
    '#type' => 'select',
    '#title' => t('Select an event'),
    '#options' => _events_dropdown_options($selected),
    '#default_value' => isset($form_state['values']['selected_event']) ? $form_state['values']['selected_event'] : '',
    '#prefix' => '<div id="bundle-filter">',
    '#suffix' => '</div>',
  );
  $form['selected_action'] = array(
    '#type' => 'select',
    '#title' => t('Select an action'),
    '#options' => $actions_options,
  );
  $form['actions'] = array(
    '#type' => 'actions',
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );


  return $form;
}

function ajax_bundle_dropdown_callback($form, &$form_state) {
  return $form['selected_event'];
}

function _events_dropdown_options($bundle = null) {
  $query = db_select('teamleader_webhook_events', 'twe')
    ->fields('twe', array('eid', 'machine_name', 'name'))
    ->condition('bundle', $bundle);
  $results = $query->execute()->fetchAll();

  $options = array();
  foreach($results as $result) {
    $options[$result->eid] = t($result->name);
  }

  return $options;
}

function teamleader_webhooks_machine_name_exists ($value) {
  // Prefix with 'field_'.
  $machine_name = 'webhook-' . $value;

  $query = db_select('teamleader_webhook_config', 'twc')
    ->fields('twc', array('cid'))
    ->condition('machine_name', $machine_name);
  $results = $query->execute()->fetchAll();

  if(count($results) > 0) {
    return true;
  }
  else {
    return false;
  }
}

function teamleader_webhooks_add_validate($form, $form_state) {
  $machine_name = $form_state['values']['machine_name'];
  if($machine_name == 'webhook-') {
    form_set_error('machine_name', t('Choose a unique machine name.'));
   }
}

function teamleader_webhooks_add_submit($form, &$form_state) {
  $label = $form_state['values']['label'];
  $machine_name = $form_state['values']['machine_name'];

  $eid = $form_state['values']['selected_event'];
  $aid = $form_state['values']['selected_action'];

  $cid = db_insert('teamleader_webhook_config')
    ->fields(array(
      'eid' => $eid,
      'aid' => $aid,
      'machine_name' => $machine_name,
      'status' => 0,
      'label' => $label,
      'weight' => 0,
    ))
    ->execute();
  drupal_set_message(t('Configuration is saved temporary. Complete the actions configuration to activate the configuration.'));
  $form_state['redirect'] = array('admin/config/teamleader/webhooks/config/' . $cid);
}

function teamleader_webhook_config($form, $form_state) {
  $cid = $form_state['build_info']['args'][0];
  $query = db_select('teamleader_webhook_events', 'twe');
  $query->join('teamleader_webhook_config', 'twc', 'twc.eid = twe.eid');
  $query->join('teamleader_webhook_actions', 'twa', 'twa.aid = twc.aid');
  $query->fields('twe', array('eid', 'machine_name', 'name', 'bundle'));
  $query->fields('twa', array('aid', 'machine_name', 'name'));
  $query->fields('twc', array('cid', 'label', 'machine_name', 'status', 'weight', 'data'));
  $query->condition('cid', $cid);

  $result = $query->execute()->fetch();
  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#default_value' => $result->label,
    '#required' => TRUE,
  );
  $form['status'] = array(
    '#type' => 'radios',
    '#title' => t('Status'),
    '#options' => array(
      0 => t('Disabled'),
      1 => t('Active'),
    ),
    '#default_value' => $result->status,
    '#description' => t('The action will only be executed when active.'),
  );
  $form['action'] = array(
    '#type' => 'fieldset',
    '#title' => t($result->twa_name),
    '#collapsible' => FALSE,
  );
  $form['action_name'] = array(
    '#type' => 'hidden',
    '#value' => $result->twa_machine_name,
  );
  switch ($result->twa_machine_name) {
    case 'send_email' :
      $form['action'][] = _email_config_form($result->bundle, $result);
    break;

  }

  $form['actions'] = array(
    '#type' => 'actions',
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

function _email_config_form($bundle, $result = null) {
  $data = unserialize($result->data);

  $elements = array();
  switch($bundle) {
    case 'contact':
    case 'company':
    case 'ticket':
      $elements['teamleader_object'] = array(
        '#type' => 'radios',
        '#title' => t('Send email to a property in Teamleader, here from a @bundle', array('@bundle' => $bundle)),
        '#default_value' => (isset($data['TL']) ? $data['TL'] : 1),
        '#options' => array(0 => t('No'), 1 => t('Yes')),
        '#description' => t('The system will try to send the e-mail to the email property field of the Teamleader object.'),
      );
      $elements['custom_email'] = array(
        '#type' => 'textfield',
        '#title' => t('Receiver'),
        '#default_value' => (isset($data['mail']) ? $data['mail'] : ''),
        '#states' => array(
          'invisible' => array(
            ':input[name="teamleader_object"]' => array('value' => 1),
          ),
          'required' => array(
            ':input[name="teamleader_object"]' => array('value' => 0),
          ),
        ),
      );
    break;
    default:
      $elements['custom_email'] = array(
        '#type' => 'textfield',
        '#title' => t('Receiver'),
        '#default_value' => (isset($data['mail']) ? $data['mail'] : ''),
        '#required' => TRUE,
      );
    break;
  }
  $elements['email_config'] = array(
    '#type' => 'fieldset',
    '#title' => t('Email configuration'),
    '#collapsible' => TRUE,
  );
  $elements['email_config']['subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#default_value' => (isset($data['subject']) ? $data['subject'] : ''),
    '#required' => TRUE,
  );
  $elements['email_config']['body'] = array(
    '#type' => 'text_format',
    '#title' => t('Body'),
    '#default_value' => (isset($data['body']) ? $data['body'] : ''),
    '#format' => (isset($data['format']) ? $data['format'] : 'full_html'),
    '#required' => TRUE,
  );
  return $elements;
}

function teamleader_webhook_config_validate($form, &$form_state) {
  if(isset($form_state['values']['teamleader_object']) && $form_state['values']['teamleader_object'] == 0) {
    if(empty($form_state['values']['custom_email'])) {
      form_set_error('custom_email', t('Receiver is required.'));
    }
  }
}

function teamleader_webhook_config_submit($form, &$form_state) {
  $cid = $form_state['build_info']['args'][0];
  $label = $form_state['values']['label'];
  $status = $form_state['values']['status'];

  $data = array();
  switch($form_state['values']['action_name']) {
    case 'send_email' :
      $data = _get_email_configuration($form_state['values']);
    break;
  }

  db_update('teamleader_webhook_config')
    ->fields(array(
      'label' => $label,
      'status' => $status,
      'data' => serialize($data),
    ))
    ->condition('cid', $cid)
    ->execute();
  drupal_set_message(t('The configuration has been saved succesfully.'));
  $form_state['redirect'] = array('admin/config/teamleader/webhooks');
}

function _get_email_configuration($values) {
  $data = array(
    'TL' => (isset($values['teamleader_object'])) ? $values['teamleader_object'] : 0,
    'mail' => $values['custom_email'],
    'subject' => $values['subject'],
    'body' => $values['body']['value'],
    'format' => $values['body']['format'],
  );
  return $data;
}
