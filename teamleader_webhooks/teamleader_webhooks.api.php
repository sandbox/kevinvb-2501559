<?php

/**
 * This contains the hooks available for this module
 */

/**
 * This function is called before all other actions are executed.
 * You can define your own action using this function and filter on the event of Teamleader.
 */
function hook_teamleader_execute_action($event) {

}
