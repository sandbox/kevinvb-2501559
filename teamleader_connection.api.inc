<?php

/**
 * Simple check if we can connect to teamleader
 */
function teamleader_connection_simple_connect() {
  $url = "https://www.teamleader.be/api/helloWorld.php";

  $fields = teamleader_connection_get_postfields();

  $ch = teamleader_connection_prepare_curl($url, $fields);

  $api_output = curl_exec($ch);
  $json_output = json_decode($api_output);

  curl_close($ch);
  if($json_output == 'Successful Teamleader API request.') {
    return TRUE;
  }
  else{
    return FALSE;
  }
}

/**
 * Prepare the fields for Teamleader
 */
function teamleader_connection_get_postfields($fields = array()) {
  $data = array(
    'api_group' => variable_get('teamleader_connection_group', ''),
    'api_secret' => variable_get('teamleader_connection_api', ''),
  );
  foreach ($fields as $key => $value) {
   $data[$key] = $value;
  }

  return $data;
}

/**
 * Prepare curl for execution to Teamleader
 */
function teamleader_connection_prepare_curl($url, $fields) {
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
  return $ch;
}
